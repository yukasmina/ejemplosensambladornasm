section .data
        resultado db "El valor extraido de la pila es:",10
        len_resultado equ $-resultado

         new_line db "",10
        len_new_line equ $-new_line
section .bss 
        datopila resb 1
section .text
        global _start
_start:
               
        
    ; Opero

        mov ax,9            ; muevo el valor de 9 a l registro ax   
        push ax             ; meto el valor a la pila que contiene (9)
        pop cx              ; extraigo el valor  de la punta de la pila  y lo ubico en cx
        add  cx,'0'         ; tranformo los digitos a caracter para presentar.
        mov [datopila],cx   ; muevo el caracter cx en la variable  datopila
       

    ; Mensaje en pantalla
        mov eax, 4
        mov ebx, 1
        mov ecx, resultado
        mov edx, len_resultado
        int 80H
     
    
    ; imprimi el valor almacenado en la pila

        mov eax, 4 ;
        mov ebx, 1
        mov ecx,datopila
        mov edx, 1 ; Se coloca siempre la longitud de caracteres
        int 80H



       ;*******************SALTO LINEA
        mov eax, 4
        mov ebx, 1
        mov ecx, new_line
        mov edx, len_new_line
        int 80H


        ;Cierro el programa
        mov eax,1
        int 80h